package com.bookbuf.library;

import com.ipudong.core.Result;

/**
 * Created by bo.wei on 2017/2/22.
 */

public class LoadingCheckAdapter extends LoadingLayoutAdapter{
    @Override
    void checkResult(Result<?> result) {
        if(result == null){
            bindEmpty();
        } else if(result.isSuccess()){
            bindNotEmpty();
        } else {
            bindError();
        }
    }
}
