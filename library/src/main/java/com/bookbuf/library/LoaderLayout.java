package com.bookbuf.library;

import android.content.Context;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.bookbuf.library.databinding.LoadingLayoutBinding;
import com.ipudong.core.Result;

/**
 * Created by bo.wei on 2017/2/15.
 * 加载布局
 */

public class LoaderLayout extends FrameLayout implements ILoadingCallback {
    LoadingLayoutBinding binding;
    private OnInitContentListener initListener;
    private OnClickListener retryListener;

    /*待切换的视图资源 id*/
    protected int mLoadingViewLayoutId;
    protected int mInitViewLayoutId;
    protected int mErrorViewLayoutId;
    protected int mEmptyViewLayoutId;
    protected int mContentViewLayoutId;


    LoadingLayoutAdapter adapter;
    DataSetObserver dataSetObserver;


    public LoaderLayout(Context context) {
        this(context, null);
    }

    public LoaderLayout(Context context, AttributeSet attrs) {
        this(context, attrs, -1);
    }

    public LoaderLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray array = context.getTheme().obtainStyledAttributes(attrs, R.styleable.LoadingLayout, 0, 0);
        if (array != null) {
            try {
                mLoadingViewLayoutId = array.getResourceId(R.styleable.LoadingLayout_loading, R.layout.content_loading);
                mInitViewLayoutId = array.getResourceId(R.styleable.LoadingLayout_init, R.layout.content_init);
                mErrorViewLayoutId = array.getResourceId(R.styleable.LoadingLayout_error, R.layout.content_error);
                mEmptyViewLayoutId = array.getResourceId(R.styleable.LoadingLayout_empty, R.layout.content_empty);
                mContentViewLayoutId = array.getResourceId(R.styleable.LoadingLayout_content, R.layout.content_content);
            } finally {
                array.recycle();
            }
        }
        init();
    }

    private void init() {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        binding = LoadingLayoutBinding.inflate(inflater, this, true);
    }

    public ViewGroup getContent() {
        return binding.layoutContent;
    }

    private void initContent(View view) {
        binding.layoutContent.removeAllViews();
        binding.layoutContent.addView(view);
    }

    private void initShade(int layoutId) {
        binding.layoutShade.removeAllViews();
        LayoutInflater inflater = LayoutInflater.from(getContext());
        inflater.inflate(layoutId, binding.layoutShade);
    }

    public void setAdapter(LoadingLayoutAdapter adapter) {
        if (this.adapter != null) {
            this.adapter.unregisterDataSetObserver(this.dataSetObserver);
            this.adapter.bindInit();
        }
        this.adapter = adapter;
        if (this.adapter != null) {
            this.dataSetObserver = new LoadingDataSetObserver(this, this.adapter);
            this.adapter.registerDataSetObserver(this.dataSetObserver);
        }
        this.adapter.notifyDataSetChanged();
    }

    public void checkResult(Result<?> result){
        adapter.checkResult(result);
    }

    public void loading(){
        adapter.bindLoading();
    }

    private View findRetryView() {
        return findViewById(R.id.loading_retry);
    }

    @Override
    public void onInit() {
        binding.layoutShade.setVisibility(VISIBLE);
        binding.layoutShade.removeAllViews();
        initShade(mInitViewLayoutId);
        initContent(initListener.onInitContent(binding.layoutContent));
    }

    @Override
    public void onEmpty() {
        binding.layoutShade.setVisibility(VISIBLE);
        initShade(mEmptyViewLayoutId);
        findRetryView().setOnClickListener(retryListener);
    }

    @Override
    public void onContent() {
        //遮罩层隐藏，显示内容
        binding.layoutShade.setVisibility(GONE);
        initListener.onComplete(binding.layoutContent.getChildAt(0));
    }

    @Override
    public void onError() {
        binding.layoutShade.setVisibility(VISIBLE);
        initShade(mErrorViewLayoutId);
        findRetryView().setOnClickListener(retryListener);
    }

    @Override
    public void onLoading() {
        binding.layoutShade.setVisibility(VISIBLE);
        initShade(mLoadingViewLayoutId);
    }

    public void destory() {
        this.adapter.unregisterDataSetObserver(this.dataSetObserver);
        this.adapter.bindInit();
        this.adapter = null;
    }

    public void setOnInitContentListener(OnInitContentListener listener, OnClickListener clickListener) {
        initListener = listener;
        retryListener = clickListener;
        setAdapter(new LoadingCheckAdapter());
    }

    /**
     * 将外部初始化的View注入到LoadLayout的content中
     */
    public interface OnInitContentListener {

        View onInitContent(ViewGroup viewGroup);

        void onComplete(View view);

    }
}
