package com.bookbuf.library;

import android.database.DataSetObserver;

import java.lang.ref.WeakReference;


/**
 *
 * Created by bo.wei on 2017/2/16.
 */

public class LoadingDataSetObserver extends DataSetObserver{
    private WeakReference<ILoadingCallback> loadingCallbackWeakReference;
    private WeakReference<LoadingLayoutAdapter> adapterWeakReference;

    public LoadingDataSetObserver(ILoadingCallback loadingCallbackWeakReference, LoadingLayoutAdapter adapterWeakReference) {
        this.loadingCallbackWeakReference = new WeakReference<>(loadingCallbackWeakReference);
        this.adapterWeakReference = new WeakReference<LoadingLayoutAdapter> (adapterWeakReference);
    }

    @Override
    public void onChanged() {
        super.onChanged();
        ILoadingCallback callback = this.loadingCallbackWeakReference.get ();
        LoadingLayoutAdapter adapter = this.adapterWeakReference.get ();
        if (adapter != null && callback != null) {
            LoadingLayoutAdapter.State state = adapter.getState ();
            switch (state) {
                case INIT:
                    callback.onInit ();
                    break;
                case LOADING:
                    callback.onLoading ();
                    break;
                case SUCCESS_EMPTY:
                    callback.onEmpty ();
                    break;
                case SUCCESS_NOT_EMPTY:
                    callback.onContent ();
                    break;
                case ERROR:
                    callback.onError ();
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public void onInvalidated() {
        super.onInvalidated();
    }
}
