package com.bookbuf.main;

import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;


import com.bookbuf.library.LoaderLayout;

import com.bookbuf.main.databinding.ActivityContentBinding;
import com.ipudong.core.Result;


import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    ContentViewModel viewModel = new ContentViewModel();
    LoaderLayout loadingLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

		/*bind view*/
        loadingLayout = (LoaderLayout) findViewById(R.id.loadingLayout);
        loadingLayout.setOnInitContentListener(new LoaderLayout.OnInitContentListener() {
            @Override
            public View onInitContent(ViewGroup viewGroup) {
                ActivityContentBinding binding = ActivityContentBinding.inflate(LayoutInflater.from(MainActivity.this), viewGroup, true);
                binding.setVm(viewModel);
                return binding.getRoot();
            }

            @Override
            public void onComplete(View view) {
                //加载完成回调
                viewModel.setTitle("hello java");

            }
        }, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadingLayout.loading();
                SimpleAsyncTask task = new SimpleAsyncTask ();
                task.setOnPostExecute (new OnPostExecute () {
                    @Override
                    public void onPostExecute (List<String> strings) {
						/*bind data*/
                        Result<String> result = new Result<>();
                        result.setSuccess(true);
                        loadingLayout.checkResult(result);
                    }
                });
                task.execute ();
            }
        });
        loadingLayout.loading();
        SimpleAsyncTask task = new SimpleAsyncTask();
        task.setOnPostExecute(new OnPostExecute() {
            @Override
            public void onPostExecute(List<String> strings) {
				/*bind data*/
                Result<String> result = new Result<>();
                result.setSuccess(true);
                loadingLayout.checkResult(result);
            }
        });
        task.execute();
    }

    private void mockWebView(WebView webView){
        webView.loadUrl("http://www.baidu.com/");
        webView.setWebViewClient(new WebViewClient() {
            Result<WebView> result = new Result<>();
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                loadingLayout.checkResult(result);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                Log.d("finish", "finish");
                result.setSuccess(true);
                loadingLayout.checkResult(result);
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                Log.d("error", "receiveError");
                result.setSuccess(false);
                loadingLayout.checkResult(result);
            }

            @Override
            public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
                super.onReceivedHttpError(view, request, errorResponse);
                Log.d("error", "httpError");
                result.setSuccess(false);
                loadingLayout.checkResult(result);
            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                super.onReceivedSslError(view, handler, error);
                Log.d("error", "sslError");
                result.setSuccess(false);
                loadingLayout.checkResult(result);
            }
        });
    }

    public void onEmptyClick(View view) {
        //开始加载
        loadingLayout.loading();
        //模拟延时
        SimpleAsyncTask task = new SimpleAsyncTask();
        task.setOnPostExecute(new OnPostExecute() {
            @Override
            public void onPostExecute(List<String> strings) {
                Result<String> result = null;
                loadingLayout.checkResult(result);
            }
        });
        task.execute();
    }

    public void onContentClick(View view) {
        //开始加载
        loadingLayout.loading();
        //模拟延时
        SimpleAsyncTask task = new SimpleAsyncTask();
        task.setOnPostExecute(new OnPostExecute() {
            @Override
            public void onPostExecute(List<String> strings) {
				/*bind data*/
                Result<String> result = new Result<>();
                result.setSuccess(true);
                loadingLayout.checkResult(result);
            }
        });
        task.execute();
    }

    public void onErrorClick(View view) {
        //开始加载
        loadingLayout.loading();
        //模拟延时
        SimpleAsyncTask task = new SimpleAsyncTask();
        task.setOnPostExecute(new OnPostExecute() {
            @Override
            public void onPostExecute(List<String> strings) {
				/*bind data*/
                Result<String> result = new Result<>();
                result.setSuccess(false);
                loadingLayout.checkResult(result);
            }
        });
        task.execute();
    }

    private class SimpleAsyncTask extends AsyncTask<String, Integer, List<String>> {

        private OnPostExecute onPostExecute;

        void setOnPostExecute(OnPostExecute onPostExecute) {
            this.onPostExecute = onPostExecute;
        }

        @Override
        protected List<String> doInBackground(String... params) {


            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            List<String> strings = new ArrayList<>();
            strings.add(" data 1");
            strings.add(" data 2");
            return strings;
        }

        @Override
        protected void onPostExecute(List<String> strings) {
            super.onPostExecute(strings);
            if (onPostExecute != null) {
                onPostExecute.onPostExecute(strings);
            }
        }
    }

    private interface OnPostExecute {
        void onPostExecute(List<String> strings);
    }
}
